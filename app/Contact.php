<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Contact extends Model
{
    use SoftDeletes;

    protected $appends = ['fullname'];


    public static function boot()
    {
		parent::boot();
    
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });

    }

    /**
     * Decrypt the user's email.
     *
     * @return string
     */
    public function getEmailAttribute($value)
    {
        return decrypt($value);
    }

    /**
     * Decrypt the user's phone.
     *
     * @return string
     */
    public function getPhoneAttribute($value)
    {
        return decrypt($value);
    }

    /**
     * Decrypt the user's first name.
     *
     * @return string
     */
    public function getFirstnameAttribute($value)
    {
        return decrypt($value);
    }

    /**
     * Decrypt the user's last name.
     *
     * @return string
     */
    public function getLastnameAttribute($value)
    {
        return decrypt($value);
    }
     /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullnameAttribute()
    {
        return $this->firstname." ".$this->lastname;
    }

    
	public function getRouteKeyName(){
    	return 'uuid';
    }

	public function getEditUrlAttribute(){
		return route('contacts.update', $this);
	}

	public function getDeleteUrlAttribute(){
		return route('contacts.delete', $this);
	}
}
