<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateContact;

class ContactController extends Controller
{

    public function index(){

        $contact = new Contact; 
        return view('contacts.index',['contacts' => $contact->get()->all()]);
    }
    
    public function create(Contact $contact){

        return view('contacts.create');
    }
    
    public function post(UpdateContact $request){

        $validated = $request->validated();
        
        $contact = New Contact;
        $contact->firstname = encrypt($validated['firstname']);
        $contact->lastname = encrypt($validated['lastname']);
        $contact->email = encrypt($validated['email']);
        $contact->phone = encrypt($validated['phone']);
        
        //save resource
        $contact->save();
     
        return redirect()->route('contacts.index')->with('alert', trans('app.saved'));
    }


    public function update(Contact $contact){

        return view('contacts.update',['contact' => $contact]);
    }
    
    public function put(Contact $contact, UpdateContact $request){

        $validated = $request->validated();
        
        //update contact resource with validated data
        $contact->firstname = encrypt($validated['firstname']);
        $contact->lastname = encrypt($validated['lastname']);
        $contact->email = encrypt($validated['email']);
        $contact->phone = encrypt($validated['phone']);
        
        //save resource
        $contact->save();
     
        return redirect()->route('contacts.index')->with('alert', trans('app.saved'));
    }
    
    public function delete(Contact $contact){
        
        $contact->delete();
    }
}
