<?php

use Illuminate\Database\Seeder;
use App\Contact;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'firstname' => encrypt('Thomas'),
            'lastname' => encrypt('Boogaard'),
            'email' => encrypt('trboogaard@gmail.com'),
            'phone' => encrypt('0629622829'),
        ]);

        Contact::create([
            'firstname' => encrypt('Kees'),
            'lastname' => encrypt('Cornelisse'),
            'email' => encrypt('kees@ef2.nl'),
            'phone' => encrypt(''),
        ]);
    }
}
