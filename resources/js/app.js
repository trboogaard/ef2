$(function() {
    
    if($("._success").length == 1){
        setTimeout(function(){
            $("._success").slideUp();
        },1500);
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$("body").on("click","._trash[data-url]",function(){

    var _el = $(this),
    _hold = _el.closest('.card').parent(),
    _url = _el.data('url'),
    _modal = $("#_deleteModal");
    
    _modal.modal("show");
    _modal.find("._confirm").off("click").on("click",function(){
        $.ajax({
            url: _url,
            type: 'DELETE',
            success: function(result) {
                _modal.modal('hide');
                _hold.remove();
            }
        });
    });

});