<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Ap Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'email' => 'Email address',
    'firstname' => 'First name',
    'lastname' => 'Last name',
    'phone' => 'Phone number',
    'submit' => 'Submit',
    'saved' => 'All changes have been saved',
    'contacts' => 'Contacts',
    'add' => 'Add new',
    'overview' => 'Overview',
    'edit' => 'Edit',
    'trash' => 'Trash',
    'confirm' => 'Confirm',
    'confirm_delete' => 'Are you sure you want do delete this contact?',
    'ok' => 'Ok',
    'cancel' => 'Cancel',

];
