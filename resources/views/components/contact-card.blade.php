<div class="card h-100">
    <div class="card-header">{{$contact->fullname}}</div>
    <div class="card-body">
        {{$contact->email}}<br>
        {{$contact->phone}}
    </div>
    <div class="card-footer">
        <a href="{{$contact->edit_url}}" class="btn btn-primary _edit">{{__('app.edit')}}</a>

        <button type="button" class="btn btn-danger float-right _trash" data-url="{{$contact->delete_url}}">{{__('app.trash')}}</button>
        
    </div>
</div>