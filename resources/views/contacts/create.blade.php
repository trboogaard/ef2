@extends('template')

@section('title', 'Add new contact')

@section('content')


    <form method="post" action="{{route('contacts.create')}}">
        @csrf

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif 
    
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="firstname">{{__('app.firstname')}}</label>
                    <input type="text" name="firstname" class="form-control" id="firstname" value="{{ Request::old('firstname') }}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="lastname">{{__('app.lastname')}}</label>
                    <input type="text" name="lastname" class="form-control" id="lastname" value="{{ Request::old('lastname') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="email">{{__('app.email')}}</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{ Request::old('email') }}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="phone">{{__('app.phone')}}</label>
                    <input type="phone" name="phone" class="form-control" id="phone" value="{{ Request::old('phone') }}">
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">{{__('app.submit')}}</button>
    
    </form>

@endsection
