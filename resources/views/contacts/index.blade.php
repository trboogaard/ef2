@extends('template')

@section('title', 'Contacts overview')

@section('content')
    
    @if (session('alert'))
        <div class="alert alert-success m-0 _success mb-3">
            {{ session('alert') }}
        </div>
    @endif

    <div class="row">
    
        @foreach ($contacts as $contact)
            <div class="col-6 col-md-4 pb-4">
                @include('components/contact-card')
            </div>
        @endforeach
    </div>

   @include('components/delete-modal')

@endsection