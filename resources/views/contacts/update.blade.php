@extends('template')

@section('title', 'Edit contact - ' . $contact->fullname)

@section('content')


    <form method="post" action="{{$contact->edit_url}}">
        @csrf
        <input name="_method" type="hidden" value="put">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif 
    

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="firstname">{{__('app.firstname')}}</label>
                    <input type="text" name="firstname" class="form-control" id="firstname" value="{{Request::old('firstname') ?? $contact->firstname}}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="lastname">{{__('app.lastname')}}</label>
                    <input type="text" name="lastname" class="form-control" id="lastname" value="{{Request::old('lastname') ?? $contact->lastname}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="email">{{__('app.email')}}</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{Request::old('email') ?? $contact->email}}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="phone">{{__('app.phone')}}</label>
                    <input type="tel" name="phone" class="form-control" id="phone" value="{{Request::old('phone') ?? $contact->phone}}">
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">{{__('app.submit')}}</button>
    
    </form>


@endsection