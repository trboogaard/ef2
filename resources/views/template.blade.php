<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title','Ef2 contacts')</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">    
    </head>
    <body>
        

        <div class="container py-4">

            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="list-group">
                        <div class="list-group-item">
                            <h3>{{__('app.contacts')}}</h3>
                        </div>
                        <a href="{{route('contacts.index')}}" class="list-group-item list-group-item-action">{{__('app.overview')}}</a>
                        <a href="{{route('contacts.create')}}" class="list-group-item list-group-item-action ">{{__('app.add')}}</a>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-9">
                    @yield('content')
                </div>
            </div>

        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
