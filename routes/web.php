<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@index')->name('contacts.index');

Route::get('/add', 'ContactController@create')->name('contacts.create');
Route::post('/add', 'ContactController@post');

Route::get('/contact/{contact}', 'ContactController@update')->name('contacts.update');
Route::delete('/contact/{contact}', 'ContactController@delete')->name('contacts.delete');
Route::put('/contact/{contact}', 'ContactController@put');

